# Description
This project contains an Android application that implements an algorithm (number of words that allows Deterministic Finite Automaton) and programmatically builds an interface for it using Kotlin DSL. 
# Algorithm
Algorithm realization -- [Main.kt](lib/src/main/kotlin/my/lib/Main.kt)

Class `Automation` takes number of edges, vertexes, string of terminal vertexes and string of transitions.

Tests -- [MainTest.kt](lib/src/test/kotlin/my/lib/MainTest.kt)
# DLS
Realization of simple Kotlin DLS to built interface programmatically -- [DLS.kt](app/src/main/java/com/example/demo/DSL.kt)

Usage example from project:
```kotlin
val view = constraintLayout {
            id = R.id.constLay
            textView {
                id = R.id.textV
                text = "Number of vertexes:"
                textSize = 24f
                height = "wrap"
                width = "wrap"
                textColor = Color.BLACK
                margins {
                    marginTop = 32
                    marginStart = 16
                }
            }

            editText {
                id = R.id.editV
                textSize = 24f
                height = "wrap"
                width = "match"
                hint = "42"
                textColor = Color.BLACK
                gravity = Gravity.END
                margins {
                    marginEnd = 8
                    marginTop = 20
                    marginStart = 8
                    startTo = R.id.textV
                }
            }

            button {
                id = R.id.buttonH
                text = "Help"
                textSize = 18f
                height = 75
                width = 126
                textColor = Color.BLACK
                onClick { helpCmnd() }
                margins {
                    marginEnd = 32
                    marginBottom = 100
                }
            }
        }
```
# Gradle
* `./gradlew assemble` build project
* `./gradlew test` build and run tests
* `./gradlew check` build and run (tests and style-checker)
* `./gradlew ktlintCheck` run style-checker (errors in `build/reports/ktlint`)
* `./gradlew ktlintFormat` format code